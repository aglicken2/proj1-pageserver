"""
  A trivial web server in Python.

  Based largely on https://docs.python.org/3.4/howto/sockets.html
  This trivial implementation is not robust:  We have omitted decent
  error handling and many other things to keep the illustration as simple
  as possible.

  FIXME:
  Currently this program always serves an ascii graphic of a cat.
  Change it to serve files if they end with .html or .css, and are
  located in ./pages  (where '.' is the directory from which this
  program is run).
"""

import config    # Configure from .ini files and command line
import logging   # Better than print statements
logging.basicConfig(format='%(levelname)s:%(message)s',
                    level=logging.INFO)
log = logging.getLogger(__name__)
# Logging level may be overridden by configuration 

import socket    # Basic TCP/IP communication on the internet
import _thread   # Response computation runs concurrently with main program
import os
import webbrowser

def listen(portnum):
    """
    Create and listen to a server socket.
    Args:
       portnum: Integer in range 1024-65535; temporary use ports
           should be in range 49152-65535.
    Returns:
       A server socket, unless connection fails (e.g., because
       the port is already in use).
    """
    # Internet, streaming socket
    serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # Bind to port and make accessible from anywhere that has our IP address
    serversocket.bind(('', portnum))
    serversocket.listen(1)    # A real server would have multiple listeners
    return serversocket


def serve(sock, func):
    """
    Respond to connections on sock.
    Args:
       sock:  A server socket, already listening on some port.
       func:  a function that takes a client socket and does something with it
    Returns: nothing
    Effects:
        For each connection, func is called on a client socket connected
        to the connected client, running concurrently in its own thread.
    """
    while True:
        log.info("Attempting to accept a connection on {}".format(sock))
        (clientsocket, address) = sock.accept()
        _thread.start_new_thread(func, (clientsocket,))


##
# Starter version only serves cat pictures. In fact, only a
# particular cat picture.  This one.
##
CAT = """
     ^ ^
   =(   )=
"""

# HTTP response codes, as the strings we will actually send.
# See:  https://en.wikipedia.org/wiki/List_of_HTTP_status_codes
# or    http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html
##
STATUS_OK = "HTTP/1.0 200 OK\n\n"
STATUS_FORBIDDEN = "HTTP/1.0 403 Forbidden\n\n"
STATUS_NOT_FOUND = "HTTP/1.0 404 Not Found\n\n"
STATUS_NOT_IMPLEMENTED = "HTTP/1.0 401 Not Implemented\n\n"


def respond(sock):
    """
    This server responds only to GET requests (not PUT, POST, or UPDATE).
    Any valid GET request is answered with an ascii graphic of a cat.
    """
    sent = 0
    request = sock.recv(1024)  # We accept only short requests
    request = str(request, encoding='utf-8', errors='strict')
    log.info("--- Received request ----")
    log.info("Request was {}\n***\n".format(request))
    
    parts = request.split()
    options = get_options()
    
    if len(parts) > 1 and (parts[0] == "GET") and ("favicon.ico" not in parts[1]):
        #http://127.0.0.1:5000/sample.html
        #http://127.0.0.1:8000/sample.html
        #http://127.0.0.1:8000/trivia.html
        #./tests/tests.sh http://127.0.0.1:8000
        
        #log.info("\n *****Here is the parts[1] ** {} **\n".format(parts[1]))
        osURL = os.path.basename(parts[1])
        #log.info("\n @Here is the osURL pre splitting ** {} **\n".format(osURL))
    
        endURL = osURL.split('.')[-1]
        #log.info("\n $Here is the endURL after splitting ** {} **\n".format(endURL))
    
        file_path = os.path.join(options.DOCROOT, osURL) #options.DOCROOT + osURL # DOCROOT+parts[1]
        # file_path = os.path.join("pages", osURL) #options.DOCROOT + osURL # DOCROOT+parts[1]
        #log.info("\n &&Here is the file_path ** {} **\n".format(file_path))
        
        if (endURL != "html") & (endURL != "css"):
            #part a If URL ends with `name.html` or `name.css`
            #log.info("\nHere is the endURL ** {} **\n".format(endURL))
            
            log.info("403 in part a not html or css")
            transmit(STATUS_FORBIDDEN, sock)

        elif ("~" in parts[1]) or ("//" in parts[1]) or (".." in parts[1]):  
            #part c If a page starts with one of the symbols(~ // ..)
            if("~" in parts[1]):
                log.info("403 ~")
            elif("//" in parts[1]):
                log.info("403 //")
            elif(".." in parts[1]):
                log.info("403 ..")    
        
            log.info("403 this is part c")
            transmit("403", sock)
            transmit(STATUS_FORBIDDEN, sock)
            
            
        elif os.path.exists(file_path) == False: 
            #part b If `name.html` is not in current directory
            
            log.info("404 this is part b")
            # log.info("404 filepath is not in current directory")
            transmit(STATUS_NOT_FOUND, sock)
            
            #transmit("\npart B Error 404 I don't handle this request: {}\n".format(request), sock)    
            
        else:
            # valid response
            transmit(STATUS_OK, sock)
            #transmit(CAT, sock)
            try: 
                with open(file_path, 'r', encoding='utf-8') as source:
                    for line in source:
                        transmit(line.strip(), sock)
            except OSError as error:
                log.warn("Failed to open or read file")
                log.warn("Requested file was {}".format(file_path))
                log.warn("Exception: {}".format(error))
    
    else:
        log.info("Unhandled request: {}".format(request))
        transmit(STATUS_NOT_IMPLEMENTED, sock)
        transmit("\nI don't handle this request: {}\n".format(request), sock)

    sock.shutdown(socket.SHUT_RDWR)
    sock.close()
    return


def transmit(msg, sock):
    """It might take several sends to get the whole message out"""
    sent = 0
    while sent < len(msg):
        buff = bytes(msg[sent:], encoding="utf-8")
        sent += sock.send(buff)

###
#
# Run from command line
#
###


def get_options():
    """
    Options from command line or configuration file.
    Returns namespace object with option value for port
    """
    # Defaults from configuration files;
    #   on conflict, the last value read has precedence
    options = config.configuration()
    # We want: PORT, DOCROOT, possibly LOGGING

    if options.PORT <= 1000:
        log.warning(("Port {} selected. " +
                         " Ports 0..1000 are reserved \n" +
                         "by the operating system").format(options.port))

    return options


def main():
    options = get_options()
    port = options.PORT
    if options.DEBUG:
        log.setLevel(logging.DEBUG)
    sock = listen(port)
    log.info("Listening on port {}".format(port))
    log.info("Socket is {}".format(sock))
    serve(sock, respond)


if __name__ == "__main__":
    main()
